<?php

/**
 *
 */
function sru_client_admin_form() {
  $form = array();

  $form['sru_client_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('SRU Client Settigns'),
    '#description' => t('This information will be use to enable SRU Client in specific content types and configuration to connect to your SRU Client server'),
  );

  $form['sru_client_settings']['sru_client_server_name'] = array(
    '#type' => 'textfield',
    '#title' => t('SRU Server Name'),
  	'#required' => TRUE,
  	'#description' => t('Human label for server name'),
    '#default_value' => variable_get('sru_client_server_name', NULL),
  );  
  
  $form['sru_client_settings']['sru_client_repository_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Repository URL'),
  	'#required' => TRUE,
  	'#description' => t('Base URL of the SRU service of the repository'),
    '#default_value' => variable_get('sru_client_repository_url', NULL),
  );
   
  $form['sru_client_settings']['sru_client_collection_names'] = array(
    '#type' => 'textfield',
    '#title' => t('Collection Name(s)'),
    '#description' => t("Name(s) of target collection(s) in a comma-separated list"),
    '#default_value' => variable_get('sru_client_collection_names', NULL),
  );
  
  $form['sru_client_settings']['sru_client_collections_ids'] = array(
    '#type' => 'textfield',
    '#title' => t('Collection ID(s)'),
    '#description' => t("ID(s) of target collections in a comma-separated list, used if collection names are empty"),
    '#default_value' => variable_get('sru_client_collections_ids', NULL),
  );
  
  $form['sru_client_settings']['sru_client_collection_operator'] = array(
	  '#type' => 'radios',
	  '#title' => t('Query must exist in collection(s) group in:'),
	  '#default_value' => variable_get('sru_client_collection_operator', ''),
	  '#options' => array(t('Some of then (OR)'),t('All of them (AND)')),
  );  
    
  $form['sru_client_settings']['sru_client_authentication_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Authentication Token'),
    '#description' => t("Authentication token string"),
    '#default_value' => variable_get('sru_client_authentication_token', NULL),
  );
  
  $form['sru_client_settings']['sru_client_schema_data'] = array(
    '#type' => 'textfield',
    '#title' => t('Schema Data'),
    '#description' => t("Schema data for records"),
    '#default_value' => variable_get('sru_client_schema_data', NULL),
  );  
  
  $form['sru_client_settings']['sru_client_max_records'] = array(
    '#type' => 'textfield',
    '#title' => t('Max Records'),  
    '#description' => t("maximum number of records to be returned in any one search"),
    '#default_value' => variable_get('sru_client_max_records', 20),
  );


  $form['posting'] = array(
    '#type' => 'fieldset',
    '#title' => t('SRU Client posting'),
    '#description' => t('Users with proper permissions will be given the option include items from SRU Client block search'),
  );

  $form['posting']['sru_client_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node types'),
    '#options' => node_get_types('names'),
    '#default_value' => variable_get('sru_client_types', array()),
  );

  $form['#submit'][] = 'intralibray_settings_submit';
  
  return system_settings_form($form);
}

/**
 * Ensure content types selected has the sru_client_item_field, if content type doesn't have must be include
 * @return unknown_type
 */
function intralibray_settings_submit($form_id, $form_values) {
	  
	module_load_include('inc', 'content', 'includes/content.crud');
	
	$label = t('SRU Client Item');
						  
	$field = array (
	    'label' => $label,
	    'field_name' => 'sru_client_item_field',
	    'type' => 'link',
	    'widget_type' => 'link',
	    'change' => 'Change basic information',
	    'weight' => '-2',
	    'description' => '',
	    'default_value' => 
	    array (
	      0 => 
	      array (
	        'title' => '',
	        'url' => '',
	      ),
	    ),
	    'default_value_php' => '',
	    'default_value_widget' => NULL,
	    'required' => 0,
	    'multiple' => '0',
	    'url' => 0,
	    'title' => 'required',
	    'title_value' => '',
	    'display' => 
	    array (
	      'url_cutoff' => '80',
	    ),
	    'attributes' => 
	    array (
	      'target' => '_blank',
	      'rel' => 'shadowbox',
	      'class' => '',
	    ),
	    'op' => 'Save field settings',
	    'module' => 'link',
	    'widget_module' => 'link',
	    'columns' => 
	    array (
	      'url' => 
	      array (
	        'type' => 'varchar',
	        'length' => 255,
	        'not null' => false,
	        'sortable' => true,
	      ),
	      'title' => 
	      array (
	        'type' => 'varchar',
	        'length' => 255,
	        'not null' => false,
	        'sortable' => true,
	      ),
	      'attributes' => 
	      array (
	        'type' => 'text',
	        'size' => 'medium',
	        'not null' => false,
	      ),
	    ),
	    'display_settings' => 
	    array (
	      'label' => 
	      array (
	        'format' => 'hidden',
	        'exclude' => 0,
	      ),
	      'teaser' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      'full' => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	      4 => 
	      array (
	        'format' => 'default',
	        'exclude' => 0,
	      ),
	    ),
	  );
			  
				  
	$types = $form_values['values']['sru_client_types'];
	
	foreach($types as $key => $value){
		//Check if content type is enabled
		$field_exist = content_field_instance_read(array('field_name' => 'sru_client_item_field', 'type_name' => $key));
      	if(empty($field_exist)){
      		if($value){
      			drupal_set_message(t("Created") . " sru_client_item_field " . t("for ") . $key);
      			$field['type_name'] = $key;
      			content_field_instance_create($field);
      		}	
      	} elseif(!$value){
      		//Delete item from content type
      		drupal_set_message(("Deleted") . " sru_client_item_field " . t("for ")  . $key);
      		content_field_instance_delete('sru_client_item_field', $key);      		
      	}
	}		
}