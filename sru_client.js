
/**
 * Present metadata in a js popup
 * @param id
 * @return
 */
function sru_client_metadata_item(id){
	var strs = Drupal.settings.srucStrings;
	
	var html = $('#' + id + ' .preview').html();
	Shadowbox.open({
        content:    html,
        player:     "html",
        title:      strs.SRU_Server,
        height:     400,
        width:      400
    });
}

/**
 * Present original resource in a js popup
 * @param id
 * @return
 */
function sru_client_preview_item(id){
	var strs = Drupal.settings.srucStrings;
	
	var link = $('#' + id + ' .preview #data_link').text();
	
	Shadowbox.open({
        content:    link,
        player:     "iframe",
        title:      strs.SRU_Server,
        height:     600,
        width:      800
    });
}

function sru_client_add_item(id){
	var strs = Drupal.settings.srucStrings;
	
	var title = $('#' + id + ' .preview #data_title').text();
	var link = $('#' + id + ' .preview #data_link').text();
	
	$('#edit-sru-client-item-field-0-title').attr('value',title);
	$('#edit-sru-client-item-field-0-url').attr('value',link);
}

if (Drupal.jsEnabled) {
	$(document).ready(function() {
						var strs = Drupal.settings.srucStrings;
					});
}